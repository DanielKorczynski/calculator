//
//  CalculatorTests.swift
//  CalculatorTests
//
//  Created by Daniel Korczyński on 18/07/16.
//  Copyright © 2016 Daniel Korczyński. All rights reserved.
//

import XCTest
@testable import Calculator

class CalculatorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        print("trace: setUp")
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let brain = CalculatorBrain()
        brain.clear()
    }
    
    override func tearDown() {
        print("trace: tearDown")
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        let brain = CalculatorBrain()
        brain.clear()
        super.tearDown()
    }
    
    func testFactorial() {
        print("trace: testFactorial")
        XCTAssertEqual(factorial(0), 1)
        XCTAssertEqual(factorial(1), 1)
        XCTAssertEqual(factorial(2), 2)
        XCTAssertEqual(factorial(3), 6)
        XCTAssertEqual(factorial(4), 24)
        XCTAssertEqual(factorial(5), 120)
        XCTAssertEqual(factorial(6), 720)
        XCTAssertEqual(factorial(7), 5040)
        XCTAssertEqual(factorial(8), 40320)
        XCTAssertEqual(factorial(9), 362880)
        XCTAssertEqual(factorial(10), 3628800)
        XCTAssertEqual(factorial(0.0), 1)
        XCTAssertEqual(factorial(0.1), 0)
        XCTAssertEqual(factorial(0.2), 0)
        XCTAssertEqual(factorial(0.3), 0)
        XCTAssertEqual(factorial(0.4), 0)
        XCTAssertEqual(factorial(0.5), 0)
        XCTAssertEqual(factorial(0.6), 0)
        XCTAssertEqual(factorial(0.7), 0)
        XCTAssertEqual(factorial(0.8), 0)
        XCTAssertEqual(factorial(0.9), 0)
        XCTAssertEqual(factorial(0.000000000000000001), 0)
        XCTAssertEqual(factorial(999.1), 0)
        XCTAssertEqual(factorial(100), 9.3326215443944102e+157)
        XCTAssertEqual(factorial(101), Double.infinity)
        XCTAssertEqual(factorial(99999999999999999999.0), Double.infinity)
        XCTAssertEqual(factorial(-0.1), 0)
        XCTAssertEqual(factorial(-0.000000000000000001), 0)
        XCTAssertEqual(factorial(-99999999999999999999.0), 0)
        XCTAssertEqual(factorial(-99999999999999999999.1), 0)
    }
    
    func testPerformConstantOperation() {
        print("trace: testPerformConstantOperation")
        let brain = CalculatorBrain()
        brain.performOperation("π")
        XCTAssertEqual(brain.result, M_PI)
    }
    
    func testPerformUnaryOperation() {
        print("trace: testPerformUnaryOperation")
        let brain = CalculatorBrain()
        brain.setOperand(6.0)
        brain.performOperation("!")
        XCTAssertEqual(brain.result, 720)
    }
    
    func testPerformBinaryOperation() {
        print("trace: testPerformBinaryOperation")
        let brain = CalculatorBrain()
        brain.setOperand(2.5)
        brain.performOperation("×")
        brain.setOperand(3.0)
        brain.performOperation("=")
        XCTAssertEqual(brain.result, 7.5)
    }
    
    func testPerformUnknownOperation() {
        print("trace: testPerformUnknownOperation")
        let brain = CalculatorBrain()
        brain.setOperand(3.5)
        brain.performOperation("unknown")
        XCTAssertEqual(brain.result, 3.5)
    }

    func testClear() {
        print("trace: testClear")
        let brain = CalculatorBrain()
        brain.setOperand(0)
        brain.setOperand(0.0)
        brain.setOperand(1.0)
        brain.setOperand(1.1)
        brain.setOperand(999999999999999999999999999999999999999.0)
        brain.setOperand(999999999999999999999999999999999999999.1)
        brain.setOperand(-0)
        brain.setOperand(-0.1)
        brain.setOperand(-1.0)
        brain.setOperand(-1.1)
        brain.clear()
        brain.setOperand(5.0)
        brain.performOperation("+")
        brain.setOperand(5.0)
        brain.performOperation("=")
        XCTAssertEqual(brain.result, 10.0)
    }
    
    func testDivideByZero() {
        print("trace: testDivideByZero")
        let brain = CalculatorBrain()
        brain.setOperand(9.0)
        brain.performOperation("÷")
        brain.setOperand(0.0)
        brain.performOperation("=")
        XCTAssertEqual(brain.result, Double.infinity)
    }
    
    func testSaveAndRestoreProgram() {
        print("trace: testProgram")
        let brain = CalculatorBrain()
        brain.setOperand(3.0)
        brain.performOperation("−")
        brain.setOperand(2.0)
        brain.performOperation("=")
        XCTAssertEqual(brain.result, 1.0)
        print("trace: save to memory")
        let memory = brain.program
        XCTAssertEqual(brain.result, 1.0)
        print("trace: clear")
        brain.clear()
        XCTAssertEqual(brain.result, 0.0)
        print("trace: restore from memory")
        brain.program = memory
        XCTAssertEqual(brain.result, 1.0)
    }
    
    func testPerformanceFactorial() {
        print("trace: testPerformanceFactorial")
        self.measure {
            for _ in 1...99999 {
                factorial(100)
            }
        }
    }
}
