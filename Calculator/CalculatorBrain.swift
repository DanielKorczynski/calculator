//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Daniel Korczyński on 18/07/16.
//  Copyright © 2016 Daniel Korczyński. All rights reserved.
//

import Foundation // there is no import UIKit because this is model, and model is UI independent

func factorial (_ x: Double) -> Double {
    if x.truncatingRemainder(dividingBy: 1) != 0 || x < 0 { // if not integer or lower than 0
        return 0
    } else if x > 100 { // if higher than 100
      return Double.infinity
    } else if x == 0 || x == 1 {
        return 1
    } else {
        return x * factorial(x-1)
    }
}

class CalculatorBrain { // class is passed by reference
    fileprivate var accumulator = 0.0
    fileprivate var internalProgram = [AnyObject]() // Double for operand, String for operation
    
    func setOperand(_ operand: Double) {
        internalProgram.append(operand as AnyObject)
        print("trace: setOperand, internalProgram: \(internalProgram)")
        accumulator = operand
        print("trace: setOperand, accumulator: \(accumulator)")
    }

    fileprivate var operations: Dictionary<String, Operation> = [
        "π" : Operation.constant(M_PI),
        "e" : Operation.constant(M_E),
        "√" : Operation.unaryOperation(sqrt),
        "sin" : Operation.unaryOperation(sin),
        "cos" : Operation.unaryOperation(cos),
        "tg" : Operation.unaryOperation(tan),
        "%" : Operation.unaryOperation({$0/100}), // closure
        "!" : Operation.unaryOperation(factorial),
        "x²": Operation.unaryOperation({$0 * $0}),
        "xⁿ": Operation.binaryOperation({pow($0, $1)}),
        "×" : Operation.binaryOperation({$0 * $1}),
        "÷" : Operation.binaryOperation({$0 / $1}),
        "+" : Operation.binaryOperation({$0 + $1}),
        "−" : Operation.binaryOperation({$0 - $1}),
        "±" : Operation.unaryOperation({-$0}),
        "=" : Operation.equals
    ]
    
    //    closure:
    //    func multiplay(op1: Double, op2: Double) -> Double { return op1 * op2 }
    //    (op1: Double, op2: Double) -> Double { return op1 * op2 }
    //    { (op1: Double, op2: Double) -> Double in return op1 * op2 }
    //    { (op1, op2) in return op1 * op2 } // removed types
    //    { ($0, $1) in return $0 * $1 } // changed to default values
    //    { return $0 * $1 } // we don't need "($0, $1) in"
    //    { $0 * $1 } // we don't need "return"
    
    fileprivate enum Operation { // enum is passed by value (copy)
        case constant(Double) // associated value Double
        case unaryOperation((Double) -> Double) // function is type like any other type, so there is no difference between function and Double
        case binaryOperation ((Double, Double) -> Double)
        case equals
    }
    
    func performOperation(_ symbol: String) {
        internalProgram.append(symbol as AnyObject)
        print("trace: performOperation, internalProgram: \(internalProgram)")
        print("trace: performOperation, symbol: \(symbol)")
        if let operation = operations[symbol] {
            print("trace: performOperation, operation: \(operation)")
            switch operation {
            case .constant(let associatedConstantValue):
                accumulator = associatedConstantValue
            case .unaryOperation(let associatedFunction):
                accumulator = associatedFunction(accumulator)
            case .binaryOperation(let associatedFunction):
                executePendingBinaryOperation()
                pending = PendingBinaryOperationInfo(binaryFunction: associatedFunction, firstOperand: accumulator)
            case .equals:
                executePendingBinaryOperation()
            }
        }
    }
    
    fileprivate func executePendingBinaryOperation() {
        if pending != nil {
            accumulator = pending!.binaryFunction(pending!.firstOperand, accumulator)
            pending = nil
        }
    }
    
    fileprivate var pending: PendingBinaryOperationInfo?
    
    fileprivate struct PendingBinaryOperationInfo { // struct like enum is passed by value (copy)
        var binaryFunction: (Double, Double) -> Double
        var firstOperand: Double
    }
    
    typealias PropertyList = AnyObject // another name for AnyObject
    var program: PropertyList {
        get {
            return internalProgram as CalculatorBrain.PropertyList
        }
        set {
            clear()
            if let arrayOfOps = newValue as? [AnyObject] {
                for op in arrayOfOps {
                    if let operand = op as? Double {
                        setOperand(operand)
                    } else if let operation = op as? String {
                        performOperation(operation)
                    }
                }
            }
        }
    }
    
    func clear() {
        accumulator = 0.0
        pending = nil
        internalProgram.removeAll()
    }
    
    var result: Double {
        get {
            print("trace: returned result: \(accumulator)")
            return accumulator
        }
    }
}
