//
//  ViewController.swift
//  Calculator
//
//  Created by Daniel Korczyński on 18/07/16.
//  Copyright © 2016 Daniel Korczyński. All rights reserved.
//

import UIKit // import module UIKit
import Foundation // for rangeOfString method

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet fileprivate weak var display: UILabel! // always initialised to not set (nil)
    fileprivate var userIsInTheMiddleOfTyping = false
    
    @IBAction fileprivate func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        print("trace: touchDigit, touched: \(digit)")
        if userIsInTheMiddleOfTyping {
            let textCurrentlyInDisplay = display.text!
            
            if textCurrentlyInDisplay.hasPrefix("0") == true
                && textCurrentlyInDisplay.characters.count == 1
                && digit == "0" {
                // do nothing if there is already 0 at the beginning of a string and next digit is 0
                print("trace: warning, only one null is allowed at the beginning of a string")
            } else if textCurrentlyInDisplay.range(of: ".") != nil && digit == "." {
                // do nothing if there is already a decimal point and next digit is a dot
                print("trace: warning, only one decimal point is allowed")
            } else if textCurrentlyInDisplay.hasPrefix("0") == true
                && textCurrentlyInDisplay.characters.count == 1
                && digit != "." {
                display.text = digit
            } else {
                display.text = textCurrentlyInDisplay + digit
            }
        } else {
            if digit == "." {
                display.text = "0."
            } else {
                display.text = digit
            }
            userIsInTheMiddleOfTyping = true
        }
    }
    
    fileprivate var displayValue: Double { // computed property
        get {
            return Double(display.text!)!
        }
        set {
            if newValue > -10e15 && newValue < 10e15 && newValue.truncatingRemainder(dividingBy: 1) == 0 {
                print ("trace: display integer value")
                display.text = String(Int(newValue))
            } else {
                print ("trace: display decimal value")
                display.text = String(newValue)
            }
        }
    }
    
    fileprivate var brain = CalculatorBrain()
    
    var savedProgram: CalculatorBrain.PropertyList?
    
    @IBAction func memorySave() {
        print("trace: memorySave")
        savedProgram = brain.program
    }
    
    @IBAction func memoryRestore() {
        print("trace: memoryRestore")
        if savedProgram != nil {
            brain.program = savedProgram!
            displayValue = brain.result
        }
    }
    
    @IBAction func clearCalc() {
        print("trace: clearCalc")
        brain.clear()
        display.text = "0"
        userIsInTheMiddleOfTyping = false
    }
    
    @IBAction fileprivate func performOperation(_ sender: UIButton) {
        if userIsInTheMiddleOfTyping {
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
        }
        if let mathematicSymbol = sender.currentTitle {
            print("trace: mathematicSymbol, touched: \(mathematicSymbol)")
            brain.performOperation(mathematicSymbol)
        }
        displayValue = brain.result
    }
}

