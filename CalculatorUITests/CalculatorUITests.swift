//
//  CalculatorUITests.swift
//  CalculatorUITests
//
//  Created by Daniel Korczyński on 18/07/16.
//  Copyright © 2016 Daniel Korczyński. All rights reserved.
//

import XCTest

class CalculatorUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.

        XCUIDevice.shared().orientation = .portrait
        XCUIDevice.shared().orientation = .faceUp
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        let app = XCUIApplication()
        app.buttons["c"].tap()
        app.staticTexts["0"].tap()
        XCUIDevice.shared().orientation = .portrait
        XCUIDevice.shared().orientation = .faceUp
        super.tearDown()
    }
    
    func testPiConstant() {
        let app = XCUIApplication()
        app.buttons["π"].tap()
        app.staticTexts["3.14159265358979"].tap()
    }
    
    func testEConstant() {
        let app = XCUIApplication()
        app.buttons["e"].tap()
        app.staticTexts["2.71828182845905"].tap()
    }
    
    func testFactorial() {
        let app = XCUIApplication()
        app.buttons["9"].tap()
        app.staticTexts["9"].tap()
        app.buttons["!"].tap()
        app.staticTexts["362880"].tap()
    }

    func testPercent() {
        let app = XCUIApplication()
        let button = app.buttons["9"]
        button.tap()
        button.tap()
        app.buttons["%"].tap()
        app.staticTexts["0.99"].tap()
    }
    
    func testSquareRoot() {
        let app = XCUIApplication()
        app.buttons["6"].tap()
        app.buttons["4"].tap()
        app.staticTexts["64"].tap()
        app.buttons["√"].tap()
        app.staticTexts["8"].tap()
    }
    
    func testSquared() {
        let app = XCUIApplication()
        app.buttons["9"].tap()
        app.buttons["x²"].tap()
        app.staticTexts["81"].tap()
    }
    
    func testPower() {
        let app = XCUIApplication()
        app.buttons["2"].tap()
        app.buttons["xⁿ"].tap()
        app.buttons["6"].tap()
        app.buttons["="].tap()
        app.staticTexts["64"].tap()
    }
    
    func testAddition() {
        let app = XCUIApplication()
        app.buttons["−"].tap()
        app.buttons["2"].tap()
        let button = app.buttons["0"]
        button.tap()
        app.buttons["+"].tap()
        app.buttons["3"].tap()
        button.tap()
        app.buttons["="].tap()
        app.staticTexts["10"].tap()
    }
    
    func testSubtraction() {
        let app = XCUIApplication()
        app.buttons["8"].tap()
        let button = app.buttons["0"]
        button.tap()
        app.buttons["−"].tap()
        app.buttons["9"].tap()
        button.tap()
        app.buttons["="].tap()
        app.staticTexts["-10"].tap()
        
    }
    
    func testMultiplication() {
        let app = XCUIApplication()
        let button = app.buttons["1"]
        button.tap()
        let button2 = app.buttons["2"]
        button2.tap()
        app.buttons["×"].tap()
        button.tap()
        button2.tap()
        app.buttons["="].tap()
        app.staticTexts["144"].tap()
        
    }
    
    func testDivision() {
        let app = XCUIApplication()
        app.buttons["4"].tap()
        app.buttons["9"].tap()
        app.buttons["÷"].tap()
        app.buttons["7"].tap()
        app.buttons["="].tap()
        app.staticTexts["7"].tap()
    }
    
    func testDecimalPoint() {
        let app = XCUIApplication()
        let button = app.buttons["."]
        button.tap()
        button.tap()
        button.tap()
        button.tap()
        button.tap()
        app.staticTexts["0."].tap()
        let button2 = app.buttons["0"]
        button2.tap()
        button2.tap()
        button2.tap()
        button2.tap()
        button2.tap()
        app.staticTexts["0.00000"].tap()
        let button3 = app.buttons["5"]
        button3.tap()
        app.staticTexts["0.000005"].tap()
        app.buttons["+"].tap()
        button3.tap()
        app.buttons["="].tap()
        app.staticTexts["5.000005"].tap()
    }
    
    func testOnlyOneZero() {
        let app = XCUIApplication()
        let button = app.buttons["0"]
        button.tap()
        button.tap()
        button.tap()
        button.tap()
        button.tap()
        app.staticTexts["0"].tap()
    }
    
    func testSignChange() {
        let app = XCUIApplication()
        app.buttons["1"].tap()
        let button = app.buttons["0"]
        button.tap()
        button.tap()
        app.staticTexts["100"].tap()
        let button2 = app.buttons["±"]
        button2.tap()
        app.staticTexts["-100"].tap()
        button2.tap()
        app.staticTexts["100"].tap()
    }
    
    func testSaveAndRestore() {
        let app = XCUIApplication()
        app.buttons["5"].tap()
        app.buttons["x²"].tap()
        let staticText = app.staticTexts["25"]
        staticText.tap()
        app.buttons["ms"].tap()
        app.buttons["c"].tap()
        app.staticTexts["0"].tap()
        app.buttons["mr"].tap()
        staticText.tap()
    }
    
    func testSine() {
        let app = XCUIApplication()
        app.buttons["3"].tap()
        app.buttons["×"].tap()
        app.buttons["π"].tap()
        app.buttons["÷"].tap()
        app.buttons["2"].tap()
        app.buttons["="].tap()
        app.buttons["sin"].tap()
        app.staticTexts["-1"].tap()
    }
    
    func testCosine() {
        let app = XCUIApplication()
        app.buttons["π"].tap()
        app.buttons["cos"].tap()
        app.staticTexts["-1"].tap()
    }
    
    func testTangent() {
        let app = XCUIApplication()
        app.buttons["π"].tap()
        app.buttons["÷"].tap()
        app.buttons["4"].tap()
        app.buttons["="].tap()
        app.buttons["tg"].tap()
        app.staticTexts["1.0"].tap()
    }
    
    func testFactorialExtended() {
        let app = XCUIApplication()
        XCTAssert(app.staticTexts["0"].exists)
        let button = app.buttons["!"]
        button.tap()
        XCTAssert(app.staticTexts["1"].exists)
        button.tap()
        XCTAssert(app.staticTexts["1"].exists)
        app.buttons["6"].tap()
        button.tap()
        XCTAssert(app.staticTexts["720"].exists)
        app.buttons["c"].tap()
        XCTAssert(app.staticTexts["0"].exists)
        app.buttons["−"].tap()
        app.buttons["5"].tap()
        app.buttons["="].tap()
        XCTAssert(app.staticTexts["-5"].exists)
        button.tap()
        XCTAssert(app.staticTexts["0"].exists)
        app.buttons["1"].tap()
        app.buttons["0"].tap()
        app.buttons["1"].tap()
        XCTAssert(app.staticTexts["101"].exists)
        button.tap()
        XCTAssert(app.staticTexts["inf"].exists)
    }
    
    func testDivideByZero() {
        let app = XCUIApplication()
        app.buttons["9"].tap()
        app.buttons["÷"].tap()
        app.buttons["0"].tap()
        app.buttons["="].tap()
        app.staticTexts["inf"].tap()
    }
    
    func testBigNumber() {
        let app = XCUIApplication()
        app.buttons["1"].tap()
        let button = app.buttons["0"]
        button.tap()
        app.buttons["xⁿ"].tap()
        app.buttons["2"].tap()
        button.tap()
        app.buttons["="].tap()
        app.staticTexts["1e+20"].tap()
        app.buttons["±"].tap()
        app.staticTexts["-1e+20"].tap()
    }    
    
}
